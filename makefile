defTest: clean buildTree clDefTest test

clean:
	del *.obj
	del *.exe
	del *.lib

buildTree:
	cl /c binary_tree.cpp
	lib binary_tree.obj

clDefTest:
	cl /c test.cpp
	link test.obj binary_tree.lib

test:
	test