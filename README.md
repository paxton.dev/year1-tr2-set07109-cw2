The makefile (built in notepad++) provided has several commands, these include:

defTest - this cleans the folder, compiles the tree, links the default test file to create a library and then runs the program against the test file.

clean - Deletes all files created by running the application.

buildTree - Compiles the whole program, minus the test file.

clDefTest - Compiles the default test file and links it to the previously compiled tree file.

test - Runs the fully compiled program.