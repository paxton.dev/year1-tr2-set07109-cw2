#include "binary_tree.h"

using namespace std;

// Creates an empty binary tree
binary_tree::binary_tree()
{

}

// Creates a binary tree with an initial value to store
binary_tree::binary_tree(int value)
{
	//Create new node
	tree = new node();
	//set new value
	(tree)->data = value;
	//Set branches to nullptr
	(tree)->left = nullptr;
	(tree)->right = nullptr;
}

// Creates a binary tree from a collection of existing values
binary_tree::binary_tree(const std::vector<int> &values)
{
	//Loops through each value in the vector and inputs each one into the tree
	for (int i = 0;i<values.size();i++)
	{
		insert(values[i]);
	}
}

//Helper function for copying an existing tree
void copier(node *&curTree, node *oldTree)
{
	//Check if nullptr. if so set new node
	if (oldTree != nullptr)
	{
		//Create new node
		curTree = new node();
		//set new value
		curTree->data = oldTree->data;
		//Set branches to nullptr
		curTree->left = nullptr;
		curTree->right = nullptr;

		//Loop through all values in the tree
		copier(curTree->left, oldTree->left);
		copier(curTree->right, oldTree->right);
	}
}

// Creates a binary tree by copying an existing tree
binary_tree::binary_tree(const binary_tree &rhs)
{
	//Call the helper function
	copier(tree, rhs.tree);
}

//Deletes the tree - freeing memory
void delete_tree(node *tree)
{
	if (tree == nullptr)
		return;
	//Loop through all left values
	delete_tree(tree->left);
	delete tree->left;
	tree->left = nullptr;
	//Loop through all right values
	delete_tree(tree->right);
	delete tree->right;
	tree->right = nullptr;
}

// Destroys (cleans up) the tree
binary_tree::~binary_tree()
{
	//Calls the helper function
	delete_tree(tree);
}

//Helper function for inserting a single value into the tree 
void insertMaker(node **tree, int value)
{
	//Check if nullptr. if so set new node
	if (*tree == nullptr)
	{
		//Create new node
		*tree = new node();
		//set new value
		(*tree)->data = value;
		//Set branches to nullptr
		(*tree)->left = nullptr;
		(*tree)->right = nullptr;
	}
	else
	{
		//Find out where to place the value and then insert it accordingly
		if (value < (*tree)->data)
			insertMaker(&((*tree)->left), value);
		else if (value > (*tree)->data)
			insertMaker(&((*tree)->right), value);
		else
			return;
	}
}

// Adds a value to the tree
void binary_tree::insert(int value)
{
	//Calls the helper function
	insertMaker(&tree, value);
}
 
//Helper function to find the minimum value
node* findMin(node* tree)
{
	while(tree->left != NULL) tree = tree->left;
	return tree;
}

//Helper function to remove a value from tree
struct node* remover(struct node *tree, int value)
{
	//Loop through all values in the tree
	if(tree == NULL) return tree;
	else if(value < tree->data) tree->left = remover(tree->left, value);
	else if(value > tree->data) tree->right = remover(tree->right, value);
	else 
	{
		// Case 1: No Child
		if(tree->left == NULL && tree->right == NULL)
		{
			delete tree;
			tree = NULL;
		} 
		// Case 2: One child
		else if(tree->left == NULL)
		{
			struct node *temp = tree;
			tree = tree->right;
			delete temp;
		} 
		else if(tree->right == NULL)
		{
			struct node *temp = tree;
			tree = tree->left;
			delete temp;
		} 
		else
		// Case 3: Two children
		{
			struct node *temp = findMin(tree->right);
			tree->data = temp->data;
			tree->right = remover(tree->right, temp->data);
		}
	}
	return tree;
}

// Removes a value from the tree
void binary_tree::remove(int value)
{
	tree = remover(tree, value);
}

//Helper function to check if a value exists in the tree
void exiCheck(node *tree, int value, bool &valExists)
{
	if (tree != nullptr)
	{
		if (tree->data == value)
		{
			valExists = true;
		}
		exiCheck(tree->left, value, valExists);
		exiCheck(tree->right, value, valExists);
	}
	return;
}

// Checks if a value is in the tree
bool binary_tree::exists(int value) const
{
	bool valExists = false;
	exiCheck(tree, value, valExists);
	return valExists;
}

//Adds the tree in order to a string.
string inorderMaker(node *tree, string *inorderString)
{
	if (tree != nullptr)
	{
		inorderMaker(tree->left, inorderString);
		*inorderString += to_string{tree->data} + " ";
		inorderMaker(tree->right, inorderString);
	}
	return *inorderString;
}

// Prints the tree to a string in numerical order
std::string binary_tree::inorder() const
{
	string inorderString = "";
	inorderString = inorderMaker(tree, &inorderString);
	//trim trailing spaces
	size_t endpos = inorderString.find_last_not_of(" \t");
	if(string::npos != endpos )
	{
		inorderString = inorderString.substr( 0, endpos+1 );
	}
	return inorderString;
}


//Adds the tree in pre-order to a string.
string preorderMaker(node *tree, string *preorderString)
{
	if (tree != nullptr)
	{
		*preorderString += to_string{tree->data} + " ";
		preorderMaker(tree->left, preorderString);
		preorderMaker(tree->right, preorderString);
	}
	return *preorderString;
}

// Prints the tree in pre-order
std::string binary_tree::preorder() const
{
	string preorderString = "";
	preorderString = preorderMaker(tree, &preorderString);
	//trim trailing spaces
	size_t endpos = preorderString.find_last_not_of(" \t");
	if(string::npos != endpos )
	{
		preorderString = preorderString.substr( 0, endpos+1 );
	}
	return preorderString;
}

//Adds the tree in post-order to a string.
string postorderMaker(node *tree, string *postorderString)
{
	if (tree != nullptr)
	{
		postorderMaker(tree->left, postorderString);
		postorderMaker(tree->right, postorderString);
		*postorderString += to_string{tree->data} + " ";
	}
	return *postorderString;
}

// Prints the tree in post-order
std::string binary_tree::postorder() const
{
	string postorderString = "";
	postorderString = postorderMaker(tree, &postorderString);
	//trim trailing spaces
	size_t endpos = postorderString.find_last_not_of(" \t");
	if(string::npos != endpos )
	{
		postorderString = postorderString.substr( 0, endpos+1 );
	}
	return postorderString;
}

// Copy assignment operator
binary_tree& binary_tree::operator=(const binary_tree &other)
{
	copier(tree, other.tree);
	return *this;
}

// Checks if two trees are equal
bool binary_tree::operator==(const binary_tree &other) const
{
	//Declaring test strings
	string t1String;
	string t2String;
	
	//Fill the test strings with the contents of each tree
	t1String = inorderMaker(tree, &t1String);
	t2String = inorderMaker(other.tree, &t2String);
	
	//If the test strings are equal this implies that the trees are equivalent
	if (t1String == t2String)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Checks if two trees are not equal (the inverse of the equivalence operator)
bool binary_tree::operator!=(const binary_tree &other) const
{
	string t1String;
	string t2String;
	
	t1String = inorderMaker(tree, &t1String);
	t2String = inorderMaker(other.tree, &t2String);
	
	if (t1String != t2String)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Inserts a new value into the binary tree
binary_tree& binary_tree::operator+(int value)
{
	insert(value);
	return *this;
}

// Removes a value from the binary tree
binary_tree& binary_tree::operator-(int value)
{
	remove(value);
	return binary_tree();
}

// Reads in values from an input stream into the tree
std::istream& operator>>(std::istream &in, binary_tree &value)
{
	int val;
	while(in.good())
	{
		in >> val;
		value.insert(val);
	}
	return in;
}

// Writes the values, in-order, to an output stream
std::ostream& operator<<(std::ostream &out, const binary_tree &value)
{
	return out << value.inorder();
}